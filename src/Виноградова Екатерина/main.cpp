#include "tpolinom.h"

int main()
{
	setlocale(LC_ALL, "Russian");
	int ms1[][2] = { { 1, 543 }, { 3, 432 }, { 5, 321 }, { 7, 210 }, { 9, 100 } };
	TPolinom poly1(ms1, 5);
	cout << "������� 1:" << endl << poly1 << endl;

	int ms2[][2] = { { 2, 643 }, { 4, 431 }, { -8, 110 }, { 10, 50 } };
	TPolinom poly2(ms2, 4);
	cout << "������� 2:" << endl << poly2 << endl;

	TPolinom sum_poly = poly1 + poly2;
	cout << "����� ���������:" << endl << sum_poly << endl;

	int x, y, z;
	cout << "������� �������� ����������:";
	cin >> x >> y >> z;
	cout << sum_poly << "=" << sum_poly.CalculatePoly(x, y, z) << endl;

	return 0;
}