#ifndef __TPOLYNOM_H__
#define __TPOLYNOM_H__


#include "THeadRing.h"
#include "TMonom.h"
#include <iostream>
using namespace std;

class TPolynom : public THeadRing 
{
public:

	TPolynom(int monoms[][2] = nullptr, int km = 0); // �����������
												  // �������� �� ������� ������������-������
	TPolynom(TPolynom &q);      // ����������� �����������

	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }

	TPolynom operator+(TPolynom &q); // �������� ���������
	TPolynom& operator=(TPolynom &q); // ������������
	bool operator==(TPolynom &q);	

	friend ostream& operator<<(ostream &os, TPolynom &poly)
	{			
		for (poly.Reset(); !poly.IsListEnded(); poly.GoNext())
		{					
			os << *poly.GetMonom() << " + ";
		}

		os << "\b\b\t ";
		return os;
	}

};

#endif // !__TPOLYNOM_H__
