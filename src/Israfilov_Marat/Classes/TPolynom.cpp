#include "TPolynom.h"
#include <iostream>

TPolynom::TPolynom(int monoms[][2], int km)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);	
	for (int i = 0; i < km; ++i)
	{
		monom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(monom);
	}
}
												 
TPolynom::TPolynom(TPolynom &q)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	int qPos = q.GetCurrentPos();

	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		PTMonom monom = q.GetMonom();
		InsLast(monom->GetCopy());		
	}

	q.SetCurrentPos(qPos);
}

TPolynom TPolynom::operator+(TPolynom &q)
{
	TPolynom result(*this);	
	PTMonom CurrMonom, AddMonom, tmp;
	result.Reset();
	q.Reset();
	while (true)
	{
		CurrMonom = result.GetMonom();
		AddMonom = q.GetMonom();
		if (CurrMonom->Index < AddMonom->Index)
		{
			tmp = new TMonom(AddMonom->Coeff, AddMonom->Index);
			result.InsCurrent(tmp);
			q.GoNext();
		}
		else if (CurrMonom->Index > AddMonom->Index)
			result.GoNext();
		else
		{
			if (CurrMonom->Index == -1)
				break;
			CurrMonom->Coeff += AddMonom->Coeff;
			if (CurrMonom->Coeff != 0)
			{
				result.GoNext();
				q.GoNext();
			}
			else
			{
				result.DelCurrent();
				q.GoNext();
			}
		}
	}


	return result;
}

TPolynom& TPolynom::operator=(TPolynom &q)
{
	if (&q != this)
	{	
		PTMonom tmp = new TMonom(0, -1);
		DelList();
		q.Reset();
		pHead->SetDatValue(tmp);

		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			tmp = q.GetMonom();
			InsLast(tmp->GetCopy());			
		}
	}

	return *this;
}

bool TPolynom::operator==(TPolynom &q)
{
	if (GetListLength() != q.GetListLength())
		return false;
	else
	{
		TPolynom poly1, poly2;
		PTMonom pMon, qMon;
		Reset(); q.Reset();
		while (!IsListEnded())
		{
			pMon = GetMonom();
			qMon = q.GetMonom();
			if (*pMon == *qMon)
			{
				GoNext(); q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
}