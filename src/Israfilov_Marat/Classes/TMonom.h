#ifndef __TMONOM_H__
#define __TMONOM_H__

#include "TDatValue.h"
#include <iostream>
using namespace std;

class TMonom;
typedef TMonom* PTMonom;

class TMonom : public TDatValue 
{
protected:

	int Coeff; // ����������� ������
	int Index; // ������ (������� ��������)

public:

	TMonom(int cval = 1, int ival = 0) 
	{
		Coeff = cval; 
		Index = ival;
	};

	virtual PTDatValue GetCopy() // ���������� �����
	{ 
		PTDatValue tmp = nullptr;
		tmp = new TMonom(Coeff, Index);
		if (tmp == nullptr)
			throw "Monom copy wasn't created";
		else		
			return tmp;
	};

	void SetCoeff(int cval) { Coeff = cval; }
	int  GetCoeff(void) { return Coeff; }
	void SetIndex(int ival) { Index = ival; }
	int  GetIndex(void) { return Index; }

	TMonom& operator=(const TMonom &tm) 
	{
		Coeff = tm.Coeff; 
		Index = tm.Index;
		return *this;
	}

	int operator==(const TMonom &tm) 
	{
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}

	int operator<(const TMonom &tm) 
	{
		return Index < tm.Index;
	}

	friend ostream& operator<<(ostream &os, TMonom &m)
	{
		os << m.GetCoeff() << "*" << "x^" << m.GetIndex() / 100 << "y^" << (m.GetIndex() % 100) / 10 << "z^" << m.GetIndex() % 10;
		return os;
	}

	friend class TPolynom;

};

#endif // !__TMONOM_H__
