﻿# Лабораторная работа №5: Полиномы

## Цели и задачи

В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:
* ввод полинома 
* организация хранения полинома
* удаление введенного ранее полинома;
* копирование полинома;
* сложение двух полиномов;
* вычисление значения полинома при заданных значениях переменных;
* вывод.

В качестве структуры хранения используются списки. В качестве дополнительной цели в лабораторной работе ставится также задача разработки общего представления списков и операций по их обработке. В числе операций над списками реализованы следующие действия:
* поддержка понятия текущего звена;
* вставка звеньев в начало, после текущей позиции и в конец списков;
* удаление звеньев в начале и в текущей позиции списков;
* организация последовательного доступа к звеньям списка (итератор).

### Условия и ограничения
При выполнении лабораторной работы использовались следующие основные предположения:
1.	Разработка структуры хранения должна быть ориентирована на представление полиномов от трех неизвестных.
2.	Степени переменных полиномов не могут превышать значения 9.
3.	Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).

### План работы
1. Разработка структуры хранения списков.
2. Разработка структуры хранения полиномов.
3. Проверка работоспособности написанных классов с помощью Google Test Framework.


### Используемые инструменты
- Система контроля версий **Git**.
- Фреймворк для написания автоматических тестов **Google Test**.
- Среда разработки **Microsoft Visual Studio 2015 Community Edition**.


## Выполнение работы

### 1. Общая структура классов:

![](classes.png)

* TDatValue - абстрактный класс объектов-значений списка
* TMonom - класс мономов

* TRootLink - базовый класс для звеньев
* TDatLink - класс для звеньев (элементов) списка с указателем на объект-значение
* TDatList - класс линейных списков
* THeadRing - класс циклических списков с заголовком
* TPolinom - класс полиномов

Для работы со списками должны быть реализованы следующие операции:
•	методы получения параметров состояния списка (проверка на пустоту, получение текущего количества звеньев);
•	метод доступа к значению первого, текущего или последнего звена;
•	методы навигации по списку (итератор);
•	методы вставки перед первым, после текущего и последнего звеньев;
•	методы удаления первого и текущего звена.

Для работы с полиномами должны быть реализованы следующие операции:
•	конструкторы инициализации и копирования;
•	метод присваивания;
•	метод сложения полиномов.


### 2. Абстрактный класс TDatValue.

DatValue.h:

    class TDatValue {
	public:
		virtual TDatValue * GetCopy() = 0; // создание копии
		~TDatValue() {}
	};

	typedef TDatValue* PTDatValue;


Monom.h:

	class TMonom;
	typedef TMonom* PTMonom;

	class TMonom : public TDatValue {
	protected:
		int Coeff; // коэффициент монома
		int Index; // индекс (свертка степеней)
	public:
		TMonom(int cval = 1, int ival = 0) {
			Coeff = cval; Index = ival;
		};
		virtual TDatValue * GetCopy() { return new TMonom(Coeff, Index); } // изготовить копию
		void SetCoeff(int cval) { Coeff = cval; }
		int  GetCoeff(void) { return Coeff; }
		void SetIndex(int ival) { Index = ival; }
		int  GetIndex(void) { return Index; }
		TMonom& operator=(const TMonom &tm) {
			Coeff = tm.Coeff; Index = tm.Index;
			return *this;
		}
		int operator==(const TMonom &tm) {
			return (Coeff == tm.Coeff) && (Index == tm.Index);
		}
		int operator<(const TMonom &tm) {
			return Index<tm.Index;
		}
		friend ostream& operator<<(ostream &os, TMonom &tm) {
			os << tm.Coeff << " " << tm.Index;
			return os;
		}
		friend class TPolinom;
	};

RootLink.h:

	class TRootLink;
	typedef TRootLink* PTRootLink;


	class TRootLink {
	protected:
		PTRootLink pNext;  // указатель на следующее звено
	public:
		TRootLink(PTRootLink pN = nullptr) { pNext = pN; }
		PTRootLink  GetNextLink() { return  pNext; }
		void SetNextLink(PTRootLink  pLink) { pNext = pLink; }
		void InsNextLink(PTRootLink  pLink) {
			PTRootLink p = pNext;  pNext = pLink;
			if (pLink != nullptr) pLink->pNext = p;
		}
		virtual void       SetDatValue(PTDatValue pVal) = 0;
		virtual PTDatValue GetDatValue() = 0;

		friend class TDatList;
	};

DatLink.h:

	class TDatLink;
	typedef TDatLink *PTDatLink;

	class TDatLink : public TRootLink {
	protected:
		PTDatValue pValue;  // указатель на объект значения
	public:
		TDatLink(PTDatValue pVal = nullptr, PTRootLink pN = nullptr) :
			TRootLink(pN) {
			pValue = pVal;
		}
		void       SetDatValue(PTDatValue pVal) { pValue = pVal; }
		PTDatValue GetDatValue() { return  pValue; }
		PTDatLink  GetNextDatLink() { return  (PTDatLink)pNext; }
		friend class TDatList;
	};

DatList.h:

	class TDatList {
	protected:
		PTDatLink pFirst;    // первое звено
		PTDatLink pLast;     // последнее звено
		PTDatLink pCurrLink; // текущее звено
		PTDatLink pPrevLink; // звено перед текущим
		PTDatLink pStop;     // значение указателя, означающего конец списка 
		int CurrPos;         // номер текущего звена (нумерация от 0)
		int ListLen;         // количество звеньев в списке
	protected:  // методы
		PTDatLink GetLink(PTDatValue pVal = nullptr, PTDatLink pLink = nullptr);
		void      DelLink(PTDatLink pLink);   // удаление звена
	public:
		TDatList();
		~TDatList() { DelList(); }
		// доступ
		PTDatValue GetDatValue(LINKPOS mode = CURRENT) const; // значение
		virtual int IsEmpty()  const { return pFirst == pStop; } // список пуст ?
		int GetListLength()    const { return ListLen; }       // к-во звеньев
															   // навигация
		int SetCurrentPos(int pos);          // установить текущее звено
		int GetCurrentPos(void) const;       // получить номер тек. звена
		virtual int Reset(void);             // установить на начало списка
		virtual int IsListEnded(void) const; // список завершен ?
		int GoNext(void);                    // сдвиг вправо текущего звена
											 // (=1 после применения GoNext для последнего звена списка)
											 // вставка звеньев
		virtual void InsFirst(PTDatValue pVal = nullptr); // перед первым
		virtual void InsLast(PTDatValue pVal = nullptr); // вставить последним 
		virtual void InsCurrent(PTDatValue pVal = nullptr); // перед текущим 
														 // удаление звеньев
		virtual void DelFirst(void);    // удалить первое звено 
		virtual void DelCurrent(void);    // удалить текущее звено 
		virtual void DelList(void);    // удалить весь список
	};
	
DatList.cpp:

	PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
	{
		return new TDatLink(pVal, pLink);
	}

	void TDatList::DelLink(PTDatLink pLink)
	{
		if (pLink != nullptr)
		{
			if (pLink->pValue != nullptr)
				delete pLink->pValue;
			delete pLink;
		}
	}

	TDatList::TDatList(): ListLen(0)
	{
		pFirst = pLast = pStop = nullptr;
		Reset();
	}

	PTDatValue TDatList::GetDatValue(LINKPOS mode) const
	{
		PTDatLink temp = nullptr;
		switch (mode)
		{
			case FIRST: 
				temp = pFirst; 
				break;
			case CURRENT: 
				temp = pCurrLink; 
				break;
			case LAST: 
				temp = pLast; 
				break;
		}
		return (temp == nullptr) ? nullptr : temp->GetDatValue();
	}


	int TDatList::Reset()
	{
		pPrevLink = pStop;
		if (IsEmpty()) {
			CurrPos = -1;
			pCurrLink = pStop;
		}
		else {
			CurrPos = 0;
			pCurrLink = pFirst;
		}
		return 1;
	}

	int TDatList::SetCurrentPos(int pos)
	{
		Reset();
		for (int i = 0; i < pos; GoNext());
		return 1;
	}

	int TDatList::GetCurrentPos() const
	{
		return CurrPos;
	}


	int TDatList::IsListEnded() const
	{
		return (pCurrLink == pStop);
	}

	int TDatList::GoNext()
	{
		if (IsListEnded())
			return 0;
		else {
			pPrevLink = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			CurrPos++;
			return 1;
		}
	}

	void TDatList::InsFirst(PTDatValue pVal)
	{
		PTDatLink DatLink = GetLink(pVal, pFirst);
		if (DatLink != nullptr) {
			//DatLink->SetNextLink(pFirst);
			pFirst = DatLink;
			ListLen++;
			if (ListLen == 1) {
				pLast = DatLink;
				Reset();
			}
			else if (CurrPos == 0) {
				pCurrLink = DatLink;
			}
			else
				CurrPos++;
		}
	}

	void TDatList::InsLast(PTDatValue pVal) {
		PTDatLink DatLink = GetLink(pVal, pStop);
		if (DatLink != nullptr)
		{
			if (pLast)
				pLast->SetNextLink(DatLink);
			pLast = DatLink;
			ListLen++;
			if (ListLen == 1)
			{
				pFirst = DatLink;
				Reset();
			}
			if (IsListEnded())
				pCurrLink = DatLink;
		}
	}

	void TDatList::InsCurrent(PTDatValue pVal ) {
		if ((pCurrLink == pFirst) || IsEmpty())
			InsFirst(pVal);
		else if (IsListEnded())
			InsLast(pVal);
		else
		{
			PTDatLink DatLink = GetLink(pVal, pCurrLink);
			if (DatLink != nullptr)
			{
				pPrevLink->SetNextLink(DatLink);
				DatLink->SetNextLink(pCurrLink);
				pCurrLink = DatLink;
				ListLen++;
			}
		}
	}

	void TDatList::DelFirst() {
		if (!IsEmpty())
		{
			PTDatLink temp = pFirst;
			pFirst = pFirst->GetNextDatLink();
			DelLink(temp);
			ListLen--;
			if (IsEmpty())
			{
				pLast = pStop;
				Reset();
			}
			else if (CurrPos == 0) pCurrLink = pFirst;
			else if (CurrPos == 1) pPrevLink = pStop;
			if (CurrPos) CurrPos--;
		}
	}

	void TDatList::DelCurrent() {
		if (pCurrLink)
		{
			if ((pCurrLink == pFirst) || IsEmpty())
				DelFirst();
			else
			{
				PTDatLink temp = pCurrLink;
				pCurrLink = pCurrLink->GetNextDatLink();
				pPrevLink->SetNextLink(pCurrLink);
				DelLink(temp);
				ListLen--;
				if (pCurrLink == pLast)
				{
					pLast = pPrevLink;
					pCurrLink = pStop;
				}
			}
		}
	}

	void TDatList::DelList() {
		while(!IsEmpty()) DelFirst();
		pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
		CurrPos = -1;
	}
	
HeadRing.h:
	
	class THeadRing : public TDatList {
	protected:
		PTDatLink pHead;     // заголовок, pFirst - звено за pHead
	public:
		THeadRing();
		~THeadRing();
		// вставка звеньев
		virtual void InsFirst(PTDatValue pVal = nullptr); // после заголовка
													   // удаление звеньев
		virtual void DelFirst(void);                 // удалить первое звено
	};


HeadRing.cpp:

	THeadRing::THeadRing(): TDatList()
	{
		InsLast();
		pHead = pFirst;
		pStop = pHead;
		Reset();
		ListLen = 0;
		pFirst->SetNextLink(pFirst);
	}

	THeadRing:: ~THeadRing()
	{
		DelList();
		DelLink(pHead);
		pHead = nullptr;
	}

	void THeadRing::InsFirst(PTDatValue pVal)
	{
		TDatList::InsFirst(pVal);
		pHead->SetNextLink(pFirst);
	}

	void THeadRing::DelFirst()
	{
		TDatList::DelFirst();
		pHead->SetNextLink(pFirst);
	}

Polinom.h:

	class TPolinom : public THeadRing {
		public:
			TPolinom(int monoms[][2] = nullptr, int km = 0); // конструктор
														  // полинома из массива «коэффициент-индекс»
			TPolinom(TPolinom &q);      // конструктор копирования
			PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
			TPolinom & operator+(TPolinom &q); // сложение полиномов
			TPolinom & operator=(TPolinom &q); // присваивание
			bool operator==(TPolinom &q);

			friend ostream& operator<<(ostream &os, TPolinom &q);
		};


Polinom.cpp:

	TPolinom::TPolinom(int monoms[][2], int km)
	{
		PTMonom Monom = new TMonom(0, -1);
		pHead->SetDatValue(Monom);
		for (int i = 0; i < km; i++)
		{
			Monom = new TMonom(monoms[i][0], monoms[i][1]);
			InsLast(Monom);
		}
	}

	TPolinom::TPolinom(TPolinom &q)
	{
		PTMonom Monom = new TMonom(0, -1);
		pHead->SetDatValue(Monom);
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			Monom = q.GetMonom();
			InsLast(Monom->GetCopy());
		}
	}

	TPolinom& TPolinom:: operator+(TPolinom &q)
	{
		PTMonom pm, qm, rm;
		q.Reset();
		Reset();

		while (true) {
			pm = GetMonom();
			qm = q.GetMonom();
			if (pm->Index < qm->Index) {
				rm = new TMonom(qm->Coeff, qm->Index);
				InsCurrent(rm);
				q.GoNext();
			}
			else if(pm->Index > qm->Index) {
				GoNext();
			}
			else {
				if (pm->Index == -1)
					break;
				pm->Coeff += qm->Coeff;
				if (pm->Coeff != 0) {
					GoNext();
					q.GoNext();
				}
				else {
					DelCurrent();
					q.GoNext();
				}
			}
		}
		return *this;
	}


	TPolinom & TPolinom:: operator=(TPolinom &q)
	{
		DelList();

		if ( (&q!=nullptr) && (&q != this) )
		{
			PTMonom Mon = new TMonom(0, -1);
			pHead->SetDatValue(Mon);
			for (q.Reset(); !q.IsListEnded(); q.GoNext())
			{
				Mon = q.GetMonom();
				InsLast(Mon->GetCopy());
			}
		}
		return *this;
	}

	ostream& operator<<(ostream &os, TPolinom &q)
	{
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
			cout << *q.GetMonom() << endl;
		return os;
	}
	
	bool TPolinom:: operator==(TPolinom &q)
	{
		bool result = false;
		if (q.GetListLength() == GetListLength())
		{
			result = true;
			for (q.Reset(), Reset(); !q.IsListEnded(); q.GoNext(), GoNext())
				if (!(*(PTMonom)q.GetDatValue() == *(PTMonom)GetDatValue()))
				{
					result = false;
					break;
				}
		}
		return result;
	}

	
	
Демонстрационная программа сложения двух полиномов:

	int main()
	{
		cout << "Polinom test" << endl;
		int ms1[][2] = { {1, 543}, {3, 432}, {5, 321}, {7, 210}, {9, 100} };
		int mn1 = sizeof(ms1) / (2 * sizeof(int));
		TPolinom p(ms1, mn1);
		cout << "Polinom 1: " << endl << p;

		int ms2[][2] = { { 2, 643 },{ 4, 431 },{ -5, 321 },{ 8, 110 },{ 10, 50 } };
		int mn2 = sizeof(ms2) / (2 * sizeof(int));
		TPolinom q(ms2, mn2);
		cout << "Polinom 2: " << endl << q;

		TPolinom r = p + q;
		cout << "Result: " << endl << r;

		return 0;
	}
	
Результат запуска демонстрационной программы:

![](demo.png)


### 3. Проверка работоспособности при помощи Google Test Framework

Данные классы были протестированы с помощью фреймворка **Google Test**.

	TEST(TPolinom, can_create_polinom_from_one_monom)
	{
		int mon[][2] = { {1, 3} };

		TPolinom A(mon, 1);

		TMonom res(1, 3);
		EXPECT_EQ(res, *A.GetMonom());
	}

	TEST(TPolinom, can_create_polinom_from_two_monoms)
	{
		const int size = 2;
		int mon[][2] = { {1, 3}, {2, 4} };

		TPolinom Pol (mon, size);

		TMonom monoms[size];
		for (int i = 0; i < size; i++)
			monoms[i] = TMonom(mon[i][0], mon[i][1]);
		for (int i = 0; i < size; i++, Pol.GoNext())
			EXPECT_EQ(monoms[i], *Pol.GetMonom());
	}

	TEST(TPolinom, can_create_pol_from_ten_monoms)
	{
		const int size = 10;
		int mon[][2] = { { 1, 3 }, { 2, 4 }, { 2, 100 }, { 3, 110 },
		{ 5, 150 }, { 6, 302 }, { 3, 400 }, { 2, 500 }, { 7 ,800 }, { 2, 888 } };
		
		TPolinom Pol(mon, size);
		
		TMonom monoms[size];
		for (int i = 0; i < size; i++)
			monoms[i] = TMonom(mon[i][0], mon[i][1]);
		for (int i = 0; i < size; i++, Pol.GoNext())
			EXPECT_EQ(monoms[i], *Pol.GetMonom());
	}

	TEST(TPolinom, can_compare_the_polynoms)
	{
		const int size = 3;
		int mon[][2] = { { 1, 3 }, { 2, 4 }, { 2, 100 } };
		TPolinom Pol1(mon, size);
		TPolinom Pol2(mon, size);

		EXPECT_TRUE(Pol1==Pol2);
	}

	TEST(TPolinom, can_copy_polinoms)
	{
		const int size = 2;
		int mon[][2] = { { 5, 3 }, { 2, 4 } };
		TPolinom Pol1(mon, size);

		TPolinom Pol2 = Pol1;

		EXPECT_TRUE(Pol1 == Pol2);
	}

	TEST(TPolinom, can_assign_polynoms)
	{
		const int size = 2;
		int mon[][2] = { { 5, 3 }, { 2, 4 } };
		TPolinom Pol1(mon, size);
		TPolinom Pol2;

		Pol2 = Pol1;

		EXPECT_TRUE(Pol1 == Pol2);
	}

	TEST(TPolinom, can_assign_empty_polynom)
	{
		TPolinom Pol1;
		TPolinom Pol2;

		Pol2 = Pol1;

		EXPECT_TRUE(Pol1 == Pol2);
	}

	TEST(TPolinom, can_add_up_linear_polynoms)
	{
		const int size = 1;
		int mon1[][2] = { { 2, 1 } };
		int mon2[][2] = { { 1, 1 } };
		// 2z
		TPolinom Pol1(mon1, size);
		// z
		TPolinom Pol2(mon2, size);

		TPolinom Pol = Pol1 + Pol2;

		const int expected_size = 1;
		int expected_mon[][2] = { { 3, 1 } };
		// 3z
		TPolinom expected_Pol(expected_mon, expected_size);
		EXPECT_TRUE(Pol == expected_Pol);
	}

	TEST(TPolinom, can_add_up_simple_polynoms)
	{
		// Arrange
		const int size1 = 3;
		const int size2 = 4;
		int mon1[][2] = { { 9, 4 } , { 8, 3 }, { 5, 2 } };
		int mon2[][2] = { { 2, 5 }, { 1, 4 }, { -8, 3 }, { 1, 1 } };
		// 5z^2+8z^3+9z^4
		TPolinom Pol1(mon1, size1);
		// z-8z^3+z^4+2z^5
		TPolinom Pol2(mon2, size2);

		TPolinom Pol = Pol1 + Pol2;

		const int expected_size = 4;
		int expected_mon[][2] = { { 2, 5 }, { 10, 4 }, { 5, 2 }, { 1, 1 } };
		// z+5z^2+10z^4+2z^5
		TPolinom expected_Pol(expected_mon, expected_size);
		EXPECT_TRUE(Pol == expected_Pol);
	}

	TEST(TPolinom, can_add_up_polynoms)
	{
		const int size1 = 5;
		const int size2 = 4;
		int mon1[][2] = { { 10, 999 }, { -21, 500 }, { 10, 432 }, { 8, 321 }, { 5, 213 } };
		int mon2[][2] = { { 20, 702 },{ 1, 500 }, { -8, 321 }, { 15, 0 } };
		// 5x^2yz^3+8x^3y^2z+10x^4y^3z^2-21x^5+10x^9y^9z^9
		TPolinom Pol1(mon1, size1);
		// 15-8x^3y^2z+x^5+20x^7z^2
		TPolinom Pol2(mon2, size2);

		TPolinom Pol = Pol1 + Pol2;

		const int expected_size = 6;
		int expected_mon[][2] = { { 10, 999 },{ 20, 702 },{ -20, 500 },{ 10, 432 }, { 5, 213 }, { 15, 0 } };
		// 15+5x^2yz^3+10x^4y^3z^2-20x^5+20x^7z^2+10x^9y^9z^9
		TPolinom expected_Pol(expected_mon, expected_size);
		EXPECT_TRUE(Pol == expected_Pol);
	}

	TEST(TPolinom, can_add_up_many_polynoms)
	{
		const int size1 = 3;
		const int size2 = 4;
		const int size3 = 3;
		int mon1[][2] = { { 9, 4 }, { 8, 3 },{ 5, 2 } };
		int mon2[][2] = { { 2, 5 }, { 1, 4 }, { -8, 3 }, { 1, 1 } };
		int mon3[][2] = { { 8, 5 }, { 2, 3 },{ 10, 0 } };
		// 5z^2+8z^3+9z^4
		TPolinom Pol1(mon1, size1);
		// z-8z^3+z^4+2z^5
		TPolinom Pol2(mon2, size2);
		// 10+2z^3+8z^5
		TPolinom Pol3(mon3, size3);

		TPolinom Pol = Pol1 + Pol2 + Pol3;

		const int expected_size = 6;
		int expected_mon[][2] = { { 10, 5 }, { 10, 4 }, { 2, 3 }, { 5, 2 }, { 1, 1 },{ 10, 0 } };
		// z+5z^2+10z^4+2z^5
		TPolinom expected_Pol(expected_mon, expected_size);
		EXPECT_TRUE(Pol == expected_Pol);
	}


**Результат**

![](tests.png)


	
## Выводы

В ходе выполнения данной работы были получены навыки создания структуры данных список (TDatList) и организации работы с полиномами на основе списков (TPolinom). 

Функциональность написанной системы была протестирована при помощи Google Test Framework. Тесты показали, что разработанная программа успешно решает поставленную в начале работы задачу.