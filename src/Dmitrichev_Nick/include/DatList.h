#pragma once
#include "DatLink.h"

#define Error_1000 1000  // SetCurrentPos( pos ), where pos is more than length of list
#define Error_1001 1001  // Removing of the link can't execute if length of list is null
enum TLinkPos { FIRST, CURRENT, LAST };

class TDatList : public TDatLink {
protected:
	PTDatLink pFirst;    // ������ �����
	PTDatLink pLast;     // ��������� �����
	PTDatLink pCurrLink; // ������� �����
	PTDatLink pPrevLink; // ����� ����� �������
	PTDatLink pStop;     // �������� ���������, ����������� ����� ������
	int CurrPos;         // ����� �������� ����� (��������� �� 0)
	int ListLen;         // ���������� ������� � ������
protected:  // ������
	PTDatLink GetLink(PTDatValue pVal = nullptr, PTDatLink pLink = nullptr);
	void      DelLink(PTDatLink pLink);   // �������� �����
public:
	TDatList();
	~TDatList() { DelList(); }
	// ������
	//using TDatLink::GetDatValue;
	PTDatValue GetDatValue(TLinkPos mode = CURRENT) const; // ��������
	virtual int IsEmpty()  const { return pFirst == pStop; } // ������ ���� ?
	int GetListLength()    const { return ListLen; }       // �-�� �������
														   // ���������
	void SetCurrentPos(int pos);          // ���������� ������� �����
	int GetCurrentPos(void) const;       // �������� ����� ���. �����
	virtual void Reset(void);            // ���������� �� ������ ������
	virtual bool IsListEnded(void) const; // ������ �������� ?
	int GoNext(void);                    // ����� ������ �������� �����
										 // (=1 ����� ���������� GoNext ��� ���������� ����� ������)
										 // ������� �������
	virtual void InsFirst(PTDatValue pVal = nullptr); // ����� ������
	virtual void InsLast(PTDatValue pVal = nullptr); // �������� ���������
	virtual void InsCurrent(PTDatValue pVal = nullptr); // ����� �������
														// �������� �������
	virtual void DelFirst(void);    // ������� ������ �����
	virtual void DelCurrent(void);    // ������� ������� �����
	virtual void DelList(void);    // ������� ���� ������
};