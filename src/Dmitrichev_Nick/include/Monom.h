#pragma once

#include "DatValue.h"
#include <iostream>		// using for printing the monoms
#include <string>		// the same

using namespace std;	// there is cout is using (below)

class TMonom;
typedef TMonom* PTMonom;

class TMonom : public TDatValue 
{
protected:
	int Coeff; // coefficient of the monom
	int Index; // convolution of the degrees
public:
	TMonom(int cval = 1, int ival = 0);
	virtual TDatValue * GetCopy();
	void SetCoeff(int);
	int  GetCoeff(void);
	void SetIndex(int);
	int  GetIndex(void);
	TMonom& operator=(const TMonom &);
	bool operator==(const TMonom &)const;
	bool operator<(const TMonom &)const;
	double calculate(double, double, double);
	friend ostream& operator<<(ostream &, TMonom &);
	friend class TPolinom;
};
