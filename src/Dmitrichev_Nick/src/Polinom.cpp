#pragma once

#include "Polinom.h"

//private////////////////////////////////////////////////////////////////////////


void TPolinom::update(void)
{
	int pos = -1;
	PTMonom curMon, polMon;
	TPolinom tempPol(*this);
	for (Reset();!IsListEnded();GoNext())
	{
		curMon = (PTMonom)GetDatValue();
		tempPol.Reset();
		while (!tempPol.IsListEnded())
		{
			if (GetCurrentPos() != tempPol.GetCurrentPos())
			{
				polMon = (PTMonom)tempPol.GetDatValue();
				if (curMon->Index == polMon->Index && curMon->Index != -1 && polMon->Index != -1)
				{
					pos = GetCurrentPos();
					curMon->Coeff += polMon->Coeff;

					SetCurrentPos(tempPol.GetCurrentPos());
					DelCurrent();
					tempPol.DelCurrent();
					SetCurrentPos(pos);
					tempPol.Reset();
				}
			}
			tempPol.GoNext();
		}
	}
	for (Reset();!IsListEnded();GoNext())
		if (((PTMonom)GetDatValue())->Coeff == 0)
			DelCurrent();
	Reset();
}

//constructers///////////////////////////////////////////////////////////////////

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom elem = new TMonom(0, -1);
	pHead->SetDatValue(elem);
	for (int i = 0; i < km; i++) 
	{
		elem = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(elem);
	}
	update();
}

TPolinom::TPolinom(TPolinom &q)
{
	PTMonom elem = new TMonom(0, -1);
	pHead->SetDatValue(elem);
	int tempPosition = q.GetCurrentPos();
	for (q.Reset(); !q.IsListEnded(); q.GoNext()) 
	{
		elem = q.GetMonom();
		InsLast(elem->GetCopy());
	}
	q.SetCurrentPos(tempPosition);
}

//operators//////////////////////////////////////////////////////////////////////

TPolinom& TPolinom::operator+(TPolinom &q)
{
	PTMonom this_mon, q_mon, new_mon;
	q.Reset();
	Reset();
	TPolinom* result=new TPolinom(*this);

	while (true)
	{
		this_mon = result->GetMonom();
		q_mon = q.GetMonom();
		if ((this_mon->operator< (*q_mon)))
		{
			new_mon = new TMonom(q_mon->Coeff, q_mon->Index);
			result->InsCurrent(new_mon);
			q.GoNext();
		}
		else if ((q_mon->operator< (*this_mon)))
			result->GoNext();
		else
		{
			if (this_mon->Index == -1)
				break;
			this_mon->Coeff += q_mon->Coeff;
			if (this_mon->Coeff != 0) {
				result->GoNext();
				q.GoNext();
			}
			else
			{
				result->DelCurrent();
				q.GoNext();
			}
		}
	}
	result->update();
	return *result;
}
TPolinom& TPolinom::operator=(TPolinom &q) // присваивание
{
	
	if ((&q) && (&q != this)) 
	{
		DelList();
		PTMonom Mon = new TMonom(0, -1);
		pHead->SetDatValue(Mon);
		int tempPosition = q.GetCurrentPos();
		for (q.Reset(); !q.IsListEnded(); q.GoNext()) 
		{
			Mon = q.GetMonom();
			InsLast(Mon->GetCopy());
		}
		q.SetCurrentPos(tempPosition);
	}
	return *this;
}

bool TPolinom::operator==(TPolinom &q)
{
	bool result = true;
	bool tempCheck = false;
	int tempPositionLeft = GetCurrentPos();
	int tempPositionRight = q.GetCurrentPos();
	if(q.GetListLength()==GetListLength())
	for (Reset();!IsListEnded();GoNext())
	{
		for (q.Reset(); !q.IsListEnded();q.GoNext())
		{		
			tempCheck = false;
			if (((PTMonom)GetDatValue())->operator==(*(PTMonom)(q.GetDatValue())))
			{
				tempCheck = true;
				break;
			}
		}
		if (!tempCheck)
			break;
	}
	result = tempCheck;
	if (q.GetListLength() == 0 && GetListLength() == 0)
		result = true;
	SetCurrentPos(tempPositionLeft);
	q.SetCurrentPos(tempPositionRight);
	return result;
}

double TPolinom::calculate(double x, double y, double z)
{
	double result = 0;
	if(ListLen!=0)
		for (Reset(); !IsListEnded(); GoNext())
			result += ((PTMonom)GetDatValue())->calculate(x, y, z);
	return result;
}
ostream & operator<<(ostream & os, TPolinom & q) {
	if (q.ListLen != 0)
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
			cout << (TMonom)(*q.GetMonom());
	else
		cout << "Polinom is empty";
	return os;
}